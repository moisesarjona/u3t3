package dam.android.moisesarjona.u3t3menuofactivities.model;

import java.io.Serializable;

import dam.android.moisesarjona.u3t3menuofactivities.R;

public class Item implements Serializable {

    // TODO Ex2 crear modelo de Item

    private int imagen;
    private String version;
    private String nombreVersion;
    private String añoLanzamiento;
    private String numeroAPI;
    private String URLWiki;

    public int getImagen() {
        return imagen;
    }

    public Item(int imagen, String version, String nombreVersion, String añoLanzamiento, String numeroAPI, String URLWiki) {
        this.imagen = imagen;
        this.version = version;
        this.nombreVersion = nombreVersion;
        this.añoLanzamiento = añoLanzamiento;
        this.numeroAPI = numeroAPI;
        this.URLWiki = URLWiki;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNombreVersion() {
        return nombreVersion;
    }

    public void setNombreVersion(String nombreVersion) {
        this.nombreVersion = nombreVersion;
    }

    public String getAñoLanzamiento() {
        return añoLanzamiento;
    }

    public void setAñoLanzamiento(String añoLanzamiento) {
        this.añoLanzamiento = añoLanzamiento;
    }

    public String getNumeroAPI() {
        return numeroAPI;
    }

    public void setNumeroAPI(String numeroAPI) {
        this.numeroAPI = numeroAPI;
    }

    public String getURLWiki() {
        return URLWiki;
    }

    public void setURLWiki(String URLWiki) {
        this.URLWiki = URLWiki;
    }

    public Item() {
        imagen = R.drawable.pacman;
    }
}
