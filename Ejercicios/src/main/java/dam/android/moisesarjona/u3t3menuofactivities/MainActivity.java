package dam.android.moisesarjona.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import dam.android.moisesarjona.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView imageViewVacio;

    private ArrayList<Item> myDataset = new ArrayList<Item>(); // TODO Ex2 creando el ArrayList de Item

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){

        imageViewVacio = findViewById(R.id.imageViewVacio);
        Button buttonAdd, buttonDeleteAll, buttonRestore;
        buttonAdd = findViewById(R.id.addOne);
        buttonDeleteAll = findViewById(R.id.deleteAll);
        buttonRestore = findViewById(R.id.restore);

        buttonAdd.setOnClickListener(v_-> addItem());
        buttonDeleteAll.setOnClickListener(v_-> delteAllItems());
        buttonRestore.setOnClickListener(v_-> restore());

        imageViewVacio.setImageResource(R.drawable.vacio);

        recyclerView = findViewById(R.id.recyclerViewActivities);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        new ItemTouchHelper(itemTouch).attachToRecyclerView(recyclerView);

        mAdapter = new MyAdapter(myDataset,this);
        recyclerView.setAdapter(mAdapter);

        createItem();
    }

    @Override
    public void onItemClick(Item activity) {

        //Toast.makeText(this, activityName, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(MainActivity.this, ItemDetailActivity.class);
        i.putExtra("activity",activity);    //TODO EX1 pasamos el nombre de la acticity con el intent
        startActivity(i);
    }

    private void addItem() {

        imageViewVacio.setVisibility(View.INVISIBLE);
        myDataset.add(new Item(R.drawable.pacman,"13.0","Nueva Version","2021","40","https://es.wikipedia.org/wiki/Android"));

        mAdapter.notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(myDataset.size());
    }

    private void delteAllItems() {

        myDataset.clear();
        imageViewVacio.setVisibility(View.VISIBLE);
        mAdapter.notifyDataSetChanged();
    }

    private void restore() {

        imageViewVacio.setVisibility(View.INVISIBLE);
        myDataset.clear();
        createItem();
        mAdapter.notifyDataSetChanged();
    }

    void createItem(){
        myDataset.add(new Item(R.drawable.android5,"5.0 – 5.1.1","Lemon Meringue Pie / lollipop","2014","21-22","https://es.wikipedia.org/wiki/Android_Lollipop"));
        myDataset.add(new Item(R.drawable.android6,"6.0 – 6.0.1","Macadamia Nut Cookie / marshmallow","2015","23","https://es.wikipedia.org/wiki/Android_Marshmallow"));
        myDataset.add(new Item(R.drawable.android7,"7.0 – 7.1.2","New York Cheesecake / nougat","2016","24 – 25","https://es.wikipedia.org/wiki/Android_Nougat"));
        myDataset.add(new Item(R.drawable.android8,"8.0 – 8.1","Oatmeal Cookie / oreo","2017","26 – 27","https://es.wikipedia.org/wiki/Android_Oreo"));
        myDataset.add(new Item(R.drawable.android9,"9.0","Android Pie","2018","28","https://es.wikipedia.org/wiki/Android_Pie"));
        myDataset.add(new Item(R.drawable.android10,"10.0","Quince Tart / Android 10","2019","29","https://es.wikipedia.org/wiki/Android_10"));
        myDataset.add(new Item(R.drawable.android11,"11.0","Red Velvet Cake / Android 11","2020","30","https://es.wikipedia.org/wiki/Android_11"));
        myDataset.add(new Item(R.drawable.android12,"12.0","Snow Cone / Android 12","2021","31","https://es.wikipedia.org/wiki/Android_12"));

    }

    ItemTouchHelper.SimpleCallback itemTouch = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            myDataset.remove(viewHolder.getAdapterPosition());
            mAdapter.notifyDataSetChanged();

            if (myDataset.isEmpty()){
                imageViewVacio.setVisibility(View.VISIBLE);
            } else
                imageViewVacio.setVisibility(View.INVISIBLE);
        }
    };


}