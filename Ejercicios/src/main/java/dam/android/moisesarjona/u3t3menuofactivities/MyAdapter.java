package dam.android.moisesarjona.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.moisesarjona.u3t3menuofactivities.model.Item;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(Item activity);
    }

    private ArrayList<Item> myDataSet;
    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewVersion, textViewApi, textViewNombre;
        ImageView imageView;

        // TODO  Ex2 seteando los elementos del cardView
        public MyViewHolder(View view) {
            super(view);
            this.imageView = view.findViewById(R.id.imageView);
            this.textViewVersion = view.findViewById(R.id.textViewVersion);
            this.textViewApi = view.findViewById(R.id.textViewApi);
            this.textViewNombre = view.findViewById(R.id.textViewNombre);
        }

        public void bind(Item activity, OnItemClickListener listener) {
            this.textViewVersion.setText(activity.getVersion());
            this.textViewApi.setText(activity.getNumeroAPI());
            this.textViewNombre.setText(activity.getNombreVersion());

            this.imageView.setImageResource(activity.getImagen());

            this.imageView.setOnClickListener(v -> listener.onItemClick(activity));
        }
    }

    MyAdapter(ArrayList<Item> myDataSet, OnItemClickListener listener) {
        this.myDataSet = myDataSet;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.card_view, parent, false); // TODO Ex2 pasando la vista

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(myDataSet.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }


}
