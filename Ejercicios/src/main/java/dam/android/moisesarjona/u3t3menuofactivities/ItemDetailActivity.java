package dam.android.moisesarjona.u3t3menuofactivities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import dam.android.moisesarjona.u3t3menuofactivities.model.Item;

public class ItemDetailActivity extends AppCompatActivity {

    TextView textViewVersion, textViewApi, textViewNombre, textViewFecha;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);


        // TODO Ex1 modificando la actionBar
        this.setTitle(R.string.TituloItemDetail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        setUi();
    }

    private void setUi() {

        Item activity = (Item) getIntent().getSerializableExtra("activity");

        this.imageView = findViewById(R.id.imageViewID);
        this.textViewVersion = findViewById(R.id.textViewVersionID);
        this.textViewApi = findViewById(R.id.textViewAPIID);
        this.textViewNombre = findViewById(R.id.textViewNombreVersionID);
        this.textViewFecha = findViewById(R.id.textViewFechaID);

        imageView.setOnClickListener(v -> openWebsite(activity));


        this.textViewVersion.setText(activity.getVersion());
        this.textViewApi.setText(activity.getNumeroAPI());
        this.textViewNombre.setText(activity.getNombreVersion());
        this.textViewFecha.setText(activity.getAñoLanzamiento());

        this.imageView.setImageResource(activity.getImagen());

    }

    private void openWebsite(Item activity) {
        Uri webpage = Uri.parse(activity.getURLWiki());
        Intent intent = new Intent(Intent.ACTION_VIEW,webpage);

        startActivity(intent);
    }


}